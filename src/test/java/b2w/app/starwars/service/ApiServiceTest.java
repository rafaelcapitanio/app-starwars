package b2w.app.starwars.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import b2w.app.starwars.client.SwapiClient;
import b2w.app.starwars.entities.PlanetEntity;
import b2w.app.starwars.exception.Messages;
import b2w.app.starwars.exception.UnableToCreatePlanetException;
import b2w.app.starwars.model.Planet;
import b2w.app.starwars.model.client.PlanetResponse;
import b2w.app.starwars.repository.PlanetRepository;

@RunWith(MockitoJUnitRunner.class)
public class ApiServiceTest {

    private final String planetFromSWapi1 = "{\n\"name\":\"Tatooine\",\n\"rotation_period\":\"23\",\n\"orbital_period\":\"304\",\n\"diameter\":\"10465\",\n\"climate\":\"arid\",\n\"gravity\":\"1standard\",\n\"terrain\":\"desert\",\n\"surface_water\":\"1\",\n\"population\":\"200000\",\n\"residents\":[\n\"http://swapi.dev/api/people/1/\",\n\"http://swapi.dev/api/people/2/\",\n\"http://swapi.dev/api/people/4/\",\n\"http://swapi.dev/api/people/6/\",\n\"http://swapi.dev/api/people/7/\",\n\"http://swapi.dev/api/people/8/\",\n\"http://swapi.dev/api/people/9/\",\n\"http://swapi.dev/api/people/11/\",\n\"http://swapi.dev/api/people/43/\",\n\"http://swapi.dev/api/people/62/\"\n],\n\"films\":[\n\"http://swapi.dev/api/films/1/\",\n\"http://swapi.dev/api/films/3/\",\n\"http://swapi.dev/api/films/4/\",\n\"http://swapi.dev/api/films/5/\",\n\"http://swapi.dev/api/films/6/\"\n],\n\"created\":\"2014-12-09T13:50:49.641000Z\",\n\"edited\":\"2014-12-20T20:58:18.411000Z\",\n\"url\":\"http://swapi.dev/api/planets/1/\"\n}";
    // private final String planetFromSWapi2 = "{\n\"name\":\"Alderaan\",\n\"rotation_period\":\"24\",\n\"orbital_period\":\"364\",\n\"diameter\":\"12500\",\n\"climate\":\"temperate\",\n\"gravity\":\"1standard\",\n\"terrain\":\"grasslands,mountains\",\n\"surface_water\":\"40\",\n\"population\":\"2000000000\",\n\"residents\":[\n\"http://swapi.dev/api/people/5/\",\n\"http://swapi.dev/api/people/68/\",\n\"http://swapi.dev/api/people/81/\"\n],\n\"films\":[\n\"http://swapi.dev/api/films/1/\",\n\"http://swapi.dev/api/films/6/\"\n],\n\"created\":\"2014-12-10T11:35:48.479000Z\",\n\"edited\":\"2014-12-20T20:58:18.420000Z\",\n\"url\":\"http://swapi.dev/api/planets/2/\"\n}";
    private final String planet1 = "{\n\"id\":1,\n\"name\":\"Tatooine\",\n\"climate\":\"arid\",\n\"terrain\":\"desert\",\n\"movieAppearances\":5\n}";
    private final String planet2 = "{\n\"id\":2,\n\"name\":\"Alderaan\",\n\"climate\":\"temperate\",\n\"terrain\":\"grasslands,mountains\",\n\"movieAppearances\":2\n}";
    private final String planet = "{\n\"id\":2,\n\"name\":\"Alderaan\",\n\"climate\":\"temperate\",\n\"terrain\":\"grasslands,mountains\"\n}";

    @InjectMocks
    private ApiService service;

    @Mock
    private SwapiClient client;

    @Mock
    private PlanetRepository repository;

    @Mock
    private Logger logger;

    @Mock
    private Messages message;

    @Test
    public void create_planet_test_1() throws JsonMappingException, JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        Planet planetMapper = mapper.readValue(planet, Planet.class);
        PlanetResponse planetSWapi = mapper.readValue(planetFromSWapi1, PlanetResponse.class);

        ResponseEntity<PlanetResponse> resp = new ResponseEntity<PlanetResponse>(planetSWapi, HttpStatus.ACCEPTED);
        when(client.getPlanet(planetMapper.getId())).thenReturn(resp);

        // PlanetEntity pEntity1 = mapper.readValue(planet1, PlanetEntity.class);
        when(repository.findById(planetMapper.getId())).thenReturn(Optional.empty()); //Optional.of(pEntity1)

        // PlanetEntity pEntity2 = mapper.readValue(planet2, PlanetEntity.class);
        when(repository.findByName(planetMapper.getName())).thenReturn(Optional.empty()); //Optional.of(pEntity2)

        service.addPlanet(planetMapper);

        verify(logger, never()).error(anyString());
    }

    @Test(expected = UnableToCreatePlanetException.class)
    public void create_planet_test_2() throws JsonMappingException, JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        Planet planetMapper = mapper.readValue(planet, Planet.class);
        PlanetResponse planetSWapi = mapper.readValue(planetFromSWapi1, PlanetResponse.class);

        ResponseEntity<PlanetResponse> resp = new ResponseEntity<PlanetResponse>(planetSWapi, HttpStatus.ACCEPTED);
        when(client.getPlanet(planetMapper.getId())).thenReturn(resp);

        PlanetEntity pEntity1 = mapper.readValue(planet1, PlanetEntity.class);
        when(repository.findById(planetMapper.getId())).thenReturn(Optional.of(pEntity1));

        PlanetEntity pEntity2 = mapper.readValue(planet2, PlanetEntity.class);
        when(repository.findByName(planetMapper.getName())).thenReturn(Optional.of(pEntity2));

        service.addPlanet(planetMapper);
    }

    @Test
    public void get_all_planets() {

        List<PlanetEntity> planets = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            PlanetEntity pEntity1 = mapper.readValue(planet1, PlanetEntity.class);
            PlanetEntity pEntity2 = mapper.readValue(planet2, PlanetEntity.class);

            planets.add(pEntity1);
            planets.add(pEntity2);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        when(repository.findAll()).thenReturn(planets);

        service.getAllPlanets();

        verify(logger, never()).error(anyString());
    }

    @Test
    public void get_planet_by_id() throws JsonMappingException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        PlanetEntity pEntity1 = mapper.readValue(planet1, PlanetEntity.class);

        when(repository.findById(Long.parseLong("1"))).thenReturn(Optional.of(pEntity1));

        service.getPlanetByValue("1");

        verify(logger, never()).error(anyString());
    }

    @Test
    public void get_planet_by_name() throws JsonMappingException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        PlanetEntity pEntity2 = mapper.readValue(planet2, PlanetEntity.class);

        when(repository.findByName("Alderaan")).thenReturn(Optional.of(pEntity2));

        service.getPlanetByValue("Alderaan");

        verify(logger, never()).error(anyString());
    }

}
