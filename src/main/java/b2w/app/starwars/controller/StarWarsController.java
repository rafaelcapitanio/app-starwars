package b2w.app.starwars.controller;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import b2w.app.starwars.exception.Messages;
import b2w.app.starwars.model.Planet;
import b2w.app.starwars.service.ApiService;

@Controller
@RequestMapping
public class StarWarsController {

    @Autowired
    private ApiService service;

    @PostMapping("/planet/create")
    public ResponseEntity<?> addPlanet(@RequestBody @NotNull Planet planet) {
        return ResponseEntity.status(HttpStatus.OK).body(service.addPlanet(planet));
    }

    @GetMapping("/planets")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.status(HttpStatus.OK).body(service.getAllPlanets());
    }

    @GetMapping("/planets/{value}")
    public ResponseEntity<?> getByName(@PathVariable @NotBlank(message = Messages.VALUE_VALIDATION) String value) {
        return ResponseEntity.status(HttpStatus.OK).body(service.getPlanetByValue(value));
    }

    @DeleteMapping("/planets/{id}")
    public ResponseEntity<?> deleteById(@PathVariable @PositiveOrZero(message = Messages.ID_VALIDATION) Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(service.removePlanet(id));
    }

}
