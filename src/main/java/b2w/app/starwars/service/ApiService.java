package b2w.app.starwars.service;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import com.mongodb.MongoException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import b2w.app.starwars.client.SwapiClient;
import b2w.app.starwars.entities.PlanetEntity;
import b2w.app.starwars.exception.Messages;
import b2w.app.starwars.exception.NotFoundException;
import b2w.app.starwars.exception.UnableToCreatePlanetException;
import b2w.app.starwars.exception.UnableToDeletePlanetException;
import b2w.app.starwars.model.Planet;
import b2w.app.starwars.model.client.PlanetResponse;
import b2w.app.starwars.model.responses.DeleteResponse;
import b2w.app.starwars.repository.PlanetRepository;

@Service
public class ApiService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SwapiClient client;

    @Autowired
    private PlanetRepository repository;

    public PlanetEntity addPlanet(Planet planet) {
        PlanetEntity entity = new PlanetEntity();

        try {

            ResponseEntity<PlanetResponse> planetInfo = client.getPlanet(planet.getId());

            if (planetInfo != null && planetInfo.hasBody()) {
                PlanetResponse planetFromSWapi = planetInfo.getBody();
                entity.setMovieAppearances(planetFromSWapi.getFilms().size());
            }

            Optional<PlanetEntity> findById = repository.findById(planet.getId());
            Optional<PlanetEntity> findByNome = repository.findByName(planet.getName());

            if (!findById.isPresent() || !findByNome.isPresent()) {
                entity.setId(planet.getId());
                entity.setClimate(planet.getClimate());
                entity.setName(planet.getName());
                entity.setTerrain(planet.getTerrain());

                repository.save(entity);
            } else {
                throw new UnableToCreatePlanetException(Messages.PLANET_ALREADY_CREATED);
            }

        } catch (Exception e) {
            logger.error(e.getStackTrace().toString());
            throw new UnableToCreatePlanetException(Messages.ERROR_TO_CREATE_PLANET);
        }

        return entity;
    }

    public DeleteResponse removePlanet(Long id) {
        repository.deleteById(id);

        return new DeleteResponse(Messages.DELETE_SUCCESS);
    }

    public PlanetEntity getPlanetByValue(String value) {
        
        if (!Pattern.matches("[a-zA-Z]+", value)) {
            return repository.findById(Long.parseLong(value)).orElseThrow(() -> throwExceptionAndLogger(Messages.INVALID_PLANET_ID));
        }

        return repository.findByName(value)
                .orElseThrow(() -> throwExceptionAndLogger(Messages.INVALID_PLANET_NAME));
    }

    public List<PlanetEntity> getAllPlanets() {

        return repository.findAll();
    }

    private NotFoundException throwExceptionAndLogger(String message) {

        logger.error(message);

        return new NotFoundException(message);

    }

}
