package b2w.app.starwars.exception;

public class Messages {

	public static final String INVALID_PLANET_ID = "Planet id does not exist";
	public static final String INVALID_PLANET_NAME = "Planet name does not exist";
	public static final String ERROR_TO_CREATE_PLANET = "Error to create a new planet";
	public static final String PLANET_ALREADY_CREATED = "Planet is already created";
	public static final String ERROR_TO_DELETE_PLANET = "Error to delete the planet";
	public static final String DELETE_SUCCESS = "Planet has been deleted";

	public static final String TERRAIN_VALIDATION = "Terrain can`t be null, empty or whitespaces";
	public static final String CLIMATE_VALIDATION = "Climate can`t be null, empty or whitespaces";
	public static final String NAME_VALIDATION = "Name can`t be null, empty or whitespaces";	
	public static final String ID_VALIDATION = "Planet ID can`t be null or negative";
	public static final String VALUE_VALIDATION = "Value to find the planet can`t be null or negative";
}
