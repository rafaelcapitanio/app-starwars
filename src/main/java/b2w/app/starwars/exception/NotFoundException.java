package b2w.app.starwars.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends IllegalArgumentException {

	public NotFoundException(String msg) {
		super(msg);
	}

	private static final long serialVersionUID = -1982323927440862743L;

}
