package b2w.app.starwars.exception;

public class UnableToDeletePlanetException extends RuntimeException {

	private static final long serialVersionUID = -6447595330890225209L;

	public UnableToDeletePlanetException(String message) {
		super(message);
	}
	
}
