package b2w.app.starwars.exception;

public class UnableToCreatePlanetException extends RuntimeException {

	private static final long serialVersionUID = -6447595330890225209L;

	public UnableToCreatePlanetException(String message) {
		super(message);
	}
	
}
