package b2w.app.starwars.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import b2w.app.starwars.model.client.Film;
import b2w.app.starwars.model.client.PlanetResponse;

@FeignClient(name = "SwapiClient", url = "${clients.swapi.url}")
public interface SwapiClient {

    @GetMapping(path = "/planets/{id}/")
    ResponseEntity<PlanetResponse> getPlanet(@PathVariable("id") Long id);

    @GetMapping(path = "/films/{id}/")
    ResponseEntity<Film> getFilm(@PathVariable("id") Long id);
}
