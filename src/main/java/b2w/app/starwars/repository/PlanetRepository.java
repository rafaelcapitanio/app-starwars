package b2w.app.starwars.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import b2w.app.starwars.entities.PlanetEntity;

public interface PlanetRepository extends MongoRepository<PlanetEntity, Long> {

    Optional<PlanetEntity> findByName(String name);
    
}
