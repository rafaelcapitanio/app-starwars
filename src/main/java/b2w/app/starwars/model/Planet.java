package b2w.app.starwars.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;

import b2w.app.starwars.exception.Messages;
import lombok.Data;

@Data
public class Planet {

    @PositiveOrZero(message = Messages.ID_VALIDATION)
    private Long id;

    @NotBlank(message = Messages.NAME_VALIDATION)
    private String name;

    @NotBlank(message = Messages.CLIMATE_VALIDATION)
    private String climate;

    @NotBlank(message = Messages.TERRAIN_VALIDATION)
    private String terrain;

}
