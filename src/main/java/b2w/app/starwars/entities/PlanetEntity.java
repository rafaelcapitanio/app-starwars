package b2w.app.starwars.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class PlanetEntity {

    @Id
    private Long id;

    private String name;

    private String climate;

    private String terrain;

    private Integer movieAppearances;

}
