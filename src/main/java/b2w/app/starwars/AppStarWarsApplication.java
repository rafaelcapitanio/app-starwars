package b2w.app.starwars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableFeignClients
@EnableCaching
@EnableMongoRepositories
public class AppStarWarsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppStarWarsApplication.class, args);
	}

}
